package cz.dat.ld30;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import org.lwjgl.openal.AL;
import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.EXTFramebufferObject;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.TextureImpl;

import cz.dat.ld30.screen.AboutScreen;
import cz.dat.ld30.screen.DeadScreen;
import cz.dat.ld30.screen.GameScreen;
import cz.dat.ld30.screen.MainMenuScreen;
import cz.dat.ld30.screen.OptionsScreen;
import cz.dat.ld30.screen.Screen;
import cz.dat.ld30.shader.ShaderLoader;
import cz.dat.ld30.sound.SoundManager;
import cz.dat.ld30.util.GLUtil;
import cz.dat.ld30.world.WorldLoader;

public class Game implements Runnable {
	public final String GAME_NAME = "Flipper";
	private final double TICK_TIME = 1.0D / 20;

	private int ticks, lastTPS, fps, fpsCounter, totalTicks;

	private long lastFPS = 0;

	private SoundManager soundManager;
	private FontManager fontManager;
	private TextureManager textureManager;
	private WorldLoader loader;

	private Screen currentScreen;
	private Map<String, Screen> screens;
	
	private int screenShader = 0;
	
	private int timeUniformID = 0;
	private int transitionUniformID = 0;

	public Game() {
	}

	@Override
	public void run() {
		this.init();
		this.screens = new HashMap<String, Screen>(4);
		this.screens.put("mainmenu", new MainMenuScreen(this));
		this.screens.put("options", new OptionsScreen(this));
		this.screens.put("game", new GameScreen(this));
		this.screens.put("diescreen", new DeadScreen(this));
		this.screens.put("about", new AboutScreen(this));
		this.soundManager.updateVolume((GameVariables.soundOn.getActualValue() == 1), 1);
		
		long time = System.nanoTime();
		long lastTime = time;
		long lastInfo = time;

		this.openScreen(this.screens.get("mainmenu"));

		while (!Display.isCloseRequested()) {
			int e = GL11.glGetError();
			if (e != 0) {
				System.err.println("GL ERROR: " + e + " - "
						+ GLU.gluErrorString(e));
			}

			time = System.nanoTime();
			while (time - lastTime >= this.TICK_TIME * 1000000000) {
				this.ticks++;
				this.tick();
				lastTime += this.TICK_TIME * 1000000000;
			}

			if (time - lastInfo >= 1000000000) {
				lastInfo += 1000000000;
				this.lastTPS = this.ticks;
				this.ticks = 0;

				this.soundManager.updatePlaying();
				this.soundManager.getMusicProvider().updateMusic(this.isInGame());
			}

			float partialTickTime = (time - lastTime)
					/ ((float) TICK_TIME * 1000000000);

			this.render(partialTickTime);

			if(GameVariables.fpsCap.getActualValue() != 0) {
				Display.sync(GameVariables.fpsCap.getActualValue());
			}
			
			Display.update();
		}

		this.exit();
	}

	private void init() {
		GLUtil.createDisplay(GameVariables.width.getFirstValue(),
				GameVariables.height.getFirstValue());
		GLUtil.initGL(GameVariables.width.getFirstValue(),
				GameVariables.height.getFirstValue());
		Display.setTitle(this.GAME_NAME);
		
		this.screenShader = ShaderLoader.loadShader("cz/dat/ld30/shader/screen.vsh", "cz/dat/ld30/shader/screen.fsh");
		
		this.timeUniformID  = ARBShaderObjects.glGetUniformLocationARB(this.screenShader, "time");
		this.transitionUniformID = ARBShaderObjects.glGetUniformLocationARB(this.screenShader, "tI");

		this.fontManager = new FontManager();
		this.soundManager = new SoundManager();
		this.textureManager = new TextureManager();
		this.loader = new WorldLoader(this);
	}

	public void exit() {
		this.soundManager.shutdown();
		
		this.currentScreen.onClosing();
		Display.destroy();
		AL.destroy();

		try {
			GameVariables.save(Main.conf);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		System.exit(0);
	}

	private void tick() {
		this.totalTicks++;
		this.currentScreen.update();
	}

	private void render(float partialTicktime) {
		GL11.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

		EXTFramebufferObject.glBindFramebufferEXT(
				EXTFramebufferObject.GL_FRAMEBUFFER_EXT, GLUtil.framebufferID);

		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

		this.currentScreen.render(partialTicktime);

		EXTFramebufferObject.glBindFramebufferEXT(
				EXTFramebufferObject.GL_FRAMEBUFFER_EXT, 0);
		
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

		GL11.glEnable(GL11.GL_TEXTURE_2D);

		ARBShaderObjects.glUseProgramObjectARB(this.screenShader);
		
		ARBShaderObjects.glUniform1fARB(timeUniformID, this.totalTicks+partialTicktime);
		
		Screen s;
		if ((s = this.currentScreen) != null && s instanceof GameScreen) {
			GameScreen gs = (GameScreen) s;
			
			if (gs.isTransitioning()) {
				
				int max = GameScreen.PORTAL_TRANSITION_TICKS;
				float current = gs.getTransitionTicks()+partialTicktime;
				
				float dist = Math.abs(max/2 - current);
				
				float tI = ((max/2-dist)*(100/(max/2f))*0.01f);
				
				ARBShaderObjects.glUniform1fARB(transitionUniformID, Math.max(0,tI));
			} else {
				ARBShaderObjects.glUniform1fARB(transitionUniformID, 0);
			}
			
		} else {
			ARBShaderObjects.glUniform1fARB(transitionUniformID, 0);
		}
		
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, GLUtil.colorTextureID);
		GL11.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glTexCoord2f(0.0f, 1.0f);
		GL11.glVertex2f(0.0f, 0.0f);
		GL11.glTexCoord2f(1.0f, 1.0f);
		GL11.glVertex2f(Display.getWidth(), 0.0f);
		GL11.glTexCoord2f(1.0f, 0.0f);
		GL11.glVertex2f(Display.getWidth(), Display.getHeight());
		GL11.glTexCoord2f(0.0f, 0.0f);
		GL11.glVertex2f(0.0f, Display.getHeight());
		GL11.glEnd();
		
		ARBShaderObjects.glUseProgramObjectARB(0);
		
		// Fix fucked up slick textures
		TextureImpl.bindNone();
		
		this.drawInfo();
		this.updateFPS();
	}

	private void drawInfo() {
		String fpsI = "FPS: " + this.getFPS();
		String tpsI = "TPS: " + this.getTPS();
		this.fontManager
				.drawString(fpsI, GameVariables.width.getFirstValue()
						- this.fontManager.getFont().getWidth(fpsI) - 5, 0,
						Color.white);
		this.fontManager.drawString(tpsI, GameVariables.width.getFirstValue()
				- this.fontManager.getFont().getWidth(tpsI) - 5,
				this.fontManager.getFont().getHeight() - 5, Color.white);
	}

	private void updateFPS() {
		if (System.nanoTime() - this.lastFPS > 1000000000) {
			this.fps = fpsCounter;
			this.fpsCounter = 0;
			this.lastFPS = System.nanoTime();
		}
		this.fpsCounter++;
	}

	public int getTPS() {
		return this.lastTPS;
	}

	public int getFPS() {
		return this.fps;
	}

	public void openScreen(Screen screen) {
		if (this.currentScreen != null) {
			this.currentScreen.onClosing();
		}

		screen.onOpening();
		Display.setTitle(this.GAME_NAME + " - " + screen.getTitle());
		this.currentScreen = screen;
	}
	
	public Screen getScreen() {
		return this.currentScreen;
	}
	
	public Screen getScreen(String name) {
		return this.screens.get(name);
	}

	public void openScreen(String screenName) {
		this.openScreen(this.screens.get(screenName));
	}

	public FontManager getFontManager() {
		return this.fontManager;
	}
	
	public TextureManager getTextureManager() {
		return this.textureManager;
	}
	
	public SoundManager getSoundManager() {
		return this.soundManager;
	}
	
	public WorldLoader getWorldLoader() {
		return this.loader;
	}
	
	public void resetGameScreen() {
		if(this.currentScreen == this.screens.get("game")) {
			this.openScreen("mainmenu");
		}
		
		this.screens.put("game", new GameScreen(this));
	}
	
	public boolean isInGame() {
		return (this.currentScreen == this.screens.get("game"));
	}
}
