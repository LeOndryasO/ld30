package cz.dat.ld30;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.util.ResourceLoader;

public class FontManager {

	public static final int FONT_BIG = 64;
	public static final int FONT_CLASSIC = 32;
	
	private TrueTypeFont defaultFont;
	private Font defaultAwtFont;
	private Map<Integer, TrueTypeFont> sizedFonts;

	public FontManager() {
		this.sizedFonts = new HashMap<>();
		this.loadDefaultFonts();
	}

	public void drawString(String text, int x, int y, int size, Color color) {
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		this.getSized(size).drawString(x, y, text, color);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
	}

	public void drawString(String text, int x, int y, Color color) {
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		this.defaultFont.drawString(x, y, text);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
	}

	public TrueTypeFont getFont() {
		return this.defaultFont;
	}

	public TrueTypeFont getSized(int size) {
		if(size == FontManager.FONT_CLASSIC) {
			return this.defaultFont;
		}

		TrueTypeFont r = this.sizedFonts.get(size);

		if(r == null) {
			TrueTypeFont n = this.loadFont(
					defaultAwtFont.deriveFont(Font.PLAIN, size), true);
			sizedFonts.put(size, n);
			return n;
		} else {
			return r;
		}
	}

	public void loadDefaultFonts() {
		try {
			this.defaultAwtFont = createFont(
					ResourceLoader.getResourceAsStream(GameVariables.resFolder
							+ "thin_pixel-7.ttf"), Font.PLAIN, FontManager.FONT_CLASSIC);
			this.defaultFont = this.loadFont(defaultAwtFont, true);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private Font createFont(InputStream in, int style, int size)
			throws FontFormatException, IOException {
		return Font.createFont(Font.TRUETYPE_FONT, in).deriveFont(style, size);
	}

	public TrueTypeFont loadFont(InputStream in, int style, int size,
			boolean antiAlias) {
		Font font = null;
		try {
			font = this.createFont(in, style, size);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return this.loadFont(font, antiAlias);
	}

	public TrueTypeFont loadFont(String name, int style, int size,
			boolean antiAlias) {
		Font font = new Font(name, style, size);
		return this.loadFont(font, antiAlias);
	}

	public TrueTypeFont loadFont(Font font, boolean antiAlias) {
		return new TrueTypeFont(font, antiAlias);
	}
}
