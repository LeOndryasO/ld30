package cz.dat.ld30;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import cz.dat.ld30.util.SettingsObject;

public class GameVariables {
	private static Map<String, SettingsObject> settings;
	
	public static SettingsObject width = new SettingsObject(1280);
	public static SettingsObject height = new SettingsObject(720);
	public static SettingsObject blockSize = new SettingsObject(32);
	public static SettingsObject coins = new SettingsObject(0);
	public static SettingsObject fpsCap = new SettingsObject(0);
	public static SettingsObject soundOn = new SettingsObject(1);
	
	public static final String resFolder = "cz/dat/res/";
	
	static {
		GameVariables.settings = new HashMap<>();
		GameVariables.settings.put("width", GameVariables.width);
		GameVariables.settings.put("height", GameVariables.height);
		GameVariables.settings.put("bs", GameVariables.blockSize);
		GameVariables.settings.put("c", GameVariables.coins);
		GameVariables.settings.put("fps", GameVariables.fpsCap);
		GameVariables.settings.put("sound", GameVariables.soundOn);
	}
	
	public static void load(File f) throws FileNotFoundException {
		Scanner s = new Scanner(f);
		
		while (s.hasNextLine()) {
			String l = s.nextLine();
			String[] words = l.split(" ");
			
			GameVariables.settings.get(words[0]).setFirstValue(Integer.parseInt(words[1]));
		}
		
		s.close();
	}
	
	public static void save(File f) throws FileNotFoundException {
		PrintWriter pw = new PrintWriter(f);
		
		for(Entry<String, SettingsObject> e : GameVariables.settings.entrySet()) {
			pw.println(e.getKey() + " " + e.getValue().getActualValue());
		}
		
		pw.close();
	}
}
