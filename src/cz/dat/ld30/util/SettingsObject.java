package cz.dat.ld30.util;

public class SettingsObject {
	private int value;
	private int startingValue;
	
	public SettingsObject(int v) {
		this.value = v;
		this.startingValue = v;
	}
	
	public int getFirstValue() {
		return this.startingValue;
	}
	
	public int getActualValue() {
		return this.value;
	}
	
	public void setValue(int value) {
		this.value = value;
	}
	
	//U can't touch dis
	public void setFirstValue(int value) {
		this.startingValue = value;
		this.setValue(value);
	}
}
