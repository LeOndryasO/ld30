package cz.dat.ld30.util;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.EXTFramebufferObject;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL14;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;

import cz.dat.ld30.GameVariables;
import cz.dat.ld30.Main;
import cz.dat.ld30.TextureManager;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.EXTFramebufferObject.*;

public class GLUtil {

	public static int colorTextureID;
	public static int framebufferID;
	public static int depthRenderBufferID;

	public static void initGL(int width, int height) {
		// init our fbo
		framebufferID = glGenFramebuffersEXT();
		colorTextureID = glGenTextures();
		depthRenderBufferID = glGenRenderbuffersEXT();

		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, framebufferID);

		glBindTexture(GL_TEXTURE_2D, colorTextureID);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8,
				GameVariables.width.getActualValue(),
				GameVariables.height.getActualValue(), 0, GL_RGBA, GL_INT,
				(java.nio.ByteBuffer) null);
		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT,
				GL_TEXTURE_2D, colorTextureID, 0);

		glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, depthRenderBufferID);
		glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT,
				GL14.GL_DEPTH_COMPONENT24,
				GameVariables.width.getActualValue(),
				GameVariables.height.getActualValue());
		glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT,
				GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT,
				depthRenderBufferID);

		int status = EXTFramebufferObject
				.glCheckFramebufferStatusEXT(EXTFramebufferObject.GL_FRAMEBUFFER_EXT);
		if (status == EXTFramebufferObject.GL_FRAMEBUFFER_COMPLETE_EXT) {
			Main.getLogger().info("Framebuffer init successful!");
		} else {
			Main.getLogger().severe("Framebuffer init failed!");
		}
		
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
		
		GLUtil.setOrtho(Display.getWidth(), Display.getHeight());
		
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_DEPTH_TEST);
	}

	public static void setOrtho(int width, int height) {
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(0, width, height, 0, 0, 1);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
	}

	public static void createDisplay(int width, int height) {
		try {
			Display.setDisplayMode(new DisplayMode(width, height));
			try {
				Display.create();
				Display.setVSyncEnabled(true);
				Main.getLogger().info("Display created");
			} catch(LWJGLException e) {
				e.printStackTrace();
			}

		} catch(LWJGLException e) {
			Main.getLogger().severe(
					"Unable to setup mode " + width + ";" + height + ", error:"
							+ e.getLocalizedMessage());
		}
	}

	public static void drawTexture(Texture texture, float textureX1,
			float textureX2, float textureY1, float textureY2, float x1,
			float x2, float y1, float y2) {
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glColor3f(1, 1, 1);

		texture.bind();
		GL11.glBegin(GL11.GL_QUADS);

		GL11.glTexCoord2f(textureX1, textureY1);
		GL11.glVertex2f(x1, y1);

		GL11.glTexCoord2f(textureX2, textureY1);
		GL11.glVertex2f(x2, y1);

		GL11.glTexCoord2f(textureX2, textureY2);
		GL11.glVertex2f(x2, y2);

		GL11.glTexCoord2f(textureX1, textureY2);
		GL11.glVertex2f(x1, y2);

		GL11.glEnd();

		GL11.glDisable(GL11.GL_TEXTURE_2D);
	}

	public static void drawTexture(Texture texture, float x, float y) {
		GLUtil.drawTexture(texture, 0, texture.getWidth(), 0,
				texture.getHeight(), x, x + texture.getImageWidth(), y, y
						+ texture.getImageHeight());
	}

	public static void drawTexture(Texture texture, float textureX1,
			float textureX2, float textureY1, float textureY2, float x, float y) {
		GLUtil.drawTexture(texture, textureX1, textureX2, textureY1, textureY2,
				x, x + texture.getImageWidth(), y, y + texture.getImageHeight());
	}

	public static void drawTexture(Texture texture, float x1, float x2,
			float y1, float y2) {
		GLUtil.drawTexture(texture, 0, texture.getWidth(), 0,
				texture.getHeight(), x1, x2, y1, y2);
	}

	public static void drawTextureCropped(Texture texture, float x, float y,
			float cropXPercent, float cropYPercent) {
		GLUtil.drawTexture(texture, 0, texture.getWidth() * cropXPercent, 0,
				texture.getHeight() * cropYPercent, x,
				x + texture.getImageWidth() * cropXPercent, y,
				y + texture.getImageHeight() * cropYPercent);
	}

	public static void drawFromAtlas(TextureManager m, int id, int image,
			float x1, float x2, float y1, float y2) {	
		GLUtil.drawTexture(m.getTexture(id), m.getX1(id, image),
				m.getX2(id, image), 0, m.getY2(id), x1, x2, y1, y2);
	}

	public static void drawFromAtlas(TextureManager m, int id, int image,
			float x, float y) {
		Coord2D ss = m.getSheetSize(id);
		GLUtil.drawFromAtlas(m, id, image, x, x + ss.x, y, y + ss.y);
	}

	public static void drawRectangle(float r, float g, float b, float a,
			float x1, float x2, float y1, float y2) {
		GL11.glColor4f(r, g, b, a);

		GL11.glDisable(GL11.GL_TEXTURE_2D);

		GL11.glBegin(GL11.GL_QUADS);

		GL11.glVertex2f(x1, y1);

		GL11.glVertex2f(x2, y1);

		GL11.glVertex2f(x2, y2);

		GL11.glVertex2f(x1, y2);

		GL11.glEnd();
	}

	public static void drawRectangle(float r, float g, float b, float x1,
			float x2, float y1, float y2) {
		GLUtil.drawRectangle(r, g, b, 1, x1, x2, y1, y2);
	}

	public static void drawRectangle(Color color, float x1, float x2, float y1,
			float y2) {
		GLUtil.drawRectangle(color.r, color.g, color.b, color.a, x1, x2, y1, y2);
	}

	public static void drawLine(float x1, float x2, float y1, float y2,
			int thickness, float r, float g, float b, float a) {
		GL11.glColor4f(r, g, b, a);
		GL11.glLineWidth(thickness);

		GL11.glBegin(GL11.GL_LINES);

		GL11.glVertex2f(x1, y1);
		GL11.glVertex2f(x2, y2);

		GL11.glEnd();
	}

	public static void drawLine(float x1, float x2, float y1, float y2,
			int thickness) {
		GLUtil.drawLine(x1, x2, y1, y2, thickness, 1, 1, 1, 1);
	}

}
