package cz.dat.ld30;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import cz.dat.ld30.util.Coord2D;


public class TextureManager {
	
	private Map<Integer, Texture> textures;
	private Map<Integer, Coord2D> sheetsSizes;
	
	public TextureManager() {
		this.textures = new HashMap<>();
		this.sheetsSizes = new HashMap<>();
		
		this.loadTextures();
	}
	
	public Texture getTexture(int id) {
		return this.textures.get(id);
	}
	
	public Coord2D getSheetSize(int id) {
		return this.sheetsSizes.get(id);
	}
	
	public void loadTextures() {
		this.addTexture(0, "logo.png");
		
		this.addTexture(1, "blocks2.png");
		this.setSheetSizeForTexture(1, new Coord2D(16, 16));
		
		this.addTexture(2, "coins.png");
		this.setSheetSizeForTexture(2, new Coord2D(32, 32));
		
		this.addTexture(3, "portal.png");
		this.setSheetSizeForTexture(3, new Coord2D(16, 16));
		
		this.addTexture(4, "pl1.png");
		this.setSheetSizeForTexture(4, new Coord2D(64, 64));
		
		this.addTexture(5, "life_full.png");
		this.addTexture(6, "life_null.png");
		
		this.addTexture(7, "pl1.png");
		this.setSheetSizeForTexture(7, new Coord2D(32, 32));
		
		this.addTexture(8, "enemy1.png");
		this.setSheetSizeForTexture(8, new Coord2D(16, 16));
		
		this.addTexture(9, "enemy2.png");
		this.setSheetSizeForTexture(9, new Coord2D(16, 16));
		
		this.addTexture(10, "czech.png");
	}
	
	public void setSheetSizeForTexture(int textureID, Coord2D size) {
		this.sheetsSizes.put(textureID, size);
	}
	
	public boolean addTexture(int id, String fileName) {
		if(textures.get(id) == null) {
			
			Texture t = this.loadTexture(fileName);
			t.bind();
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D,
					GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D,
					GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
			GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
			
			textures.put(id, t);
		}
		return false;
	}
	
	public Texture loadTexture(String fileName) {
		try {
			Texture tex = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream(GameVariables.resFolder + fileName));
			Main.getLogger().info("Successfully loaded texture " + fileName);
			return tex;
		} catch (IOException e) {
			System.err.println("Can't load texture " + fileName + ", perhaps the file doesn't exist?");
			System.exit(1);
		}
		return null;
	}
	
	public float getX1(int textureID, int image) {
		
		Texture t = this.textures.get(textureID);
		
		float pos = (this.sheetsSizes.get(textureID).x * (image - 1));
		
		float posFloat = (pos / t.getImageWidth()) * ((float) t.getImageWidth() / t.getTextureWidth());
		
		return posFloat;
	}
	
	public float getX2(int textureID, int image) {
		Texture t = this.textures.get(textureID);
		
		float pos = (this.sheetsSizes.get(textureID).x * (image));
		
		float posFloat = pos / t.getImageWidth() * ((float) t.getImageWidth() / t.getTextureWidth());
		
		return posFloat;
	}
	
	public float getY2(int textureID) {
		
		Texture t = textures.get(textureID);
		
		return t.getHeight();
	}
}
