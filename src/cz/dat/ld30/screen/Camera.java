package cz.dat.ld30.screen;

import org.lwjgl.opengl.Display;

import cz.dat.ld30.GameVariables;
import cz.dat.ld30.entity.Entity;

public class Camera {

	private int tX = 0;
	private int tY = 0;
	
	private float offsetX;
	private float offsetY;
	private Entity toAttach;	
	
	public Camera(Entity e, float offsetX, float offsetY) {
		this.toAttach = e;
		this.offsetX = offsetX;
		this.offsetY = offsetY;
	}

	public void calcTranslation() {
		int bs = GameVariables.blockSize.getActualValue();
		
		float eCenterX = (this.toAttach.getRenderBB().x1 - this.toAttach.getRenderBB().x0)/2f;
		float eCenterY = (this.toAttach.getRenderBB().y1 - this.toAttach.getRenderBB().y0)/2f;
		
		int screenCenterX = Display.getWidth()/2;
		int screenCenterY = Display.getHeight()/2;
		
		this.tX = (int) ((this.toAttach.getRenderBB().x0 + eCenterX + offsetX)* bs - screenCenterX);
		this.tY = (int) ((this.toAttach.getRenderBB().y0 + eCenterY + offsetY)* bs - screenCenterY);
	}

	public float getOffsetX() {
		return offsetX;
	}


	public void setOffsetX(float offsetX) {
		this.offsetX = offsetX;
	}


	public float getOffsetY() {
		return offsetY;
	}


	public void setOffsetY(float offsetY) {
		this.offsetY = offsetY;
	}

	public int gettX() {
		return tX;
	}

	public int gettY() {
		return tY;
	}

}
