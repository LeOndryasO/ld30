package cz.dat.ld30.screen;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;

import cz.dat.ld30.Game;
import cz.dat.ld30.GameVariables;
import cz.dat.ld30.TextureManager;
import cz.dat.ld30.entity.Entity;
import cz.dat.ld30.entity.Player;
import cz.dat.ld30.util.GLUtil;
import cz.dat.ld30.world.Block;
import cz.dat.ld30.world.Hint;
import cz.dat.ld30.world.Level;
import cz.dat.ld30.world.World;
import cz.dat.ld30.world.WorldLoader;

public class GameScreen extends Screen {

	public static final int PORTAL_TRANSITION_TICKS = 30;

	private WorldLoader loader;
	private World currentWorld;
	private Level currentLevel;

	private Camera camera;

	private int levels;
	private int currentWorldID = 0;
	private int currentLevelID = 0;

	private boolean transitioning = false;
	private int transitionTicks = 0;

	private boolean isEnd = false;

	private int lifesX, lifesY;

	public GameScreen(Game game) {
		super("Game", game);
		loader = game.getWorldLoader();

		loader.loadLevels();
		this.currentLevel = loader.getLoadedLevels().get(0);
		this.currentWorld = this.currentLevel.getWorld(0);
		this.levels = this.loader.getLoadedLevels().size();

		this.camera = new Camera(this.currentWorld.getPlayer(), 1, 0);

		this.lifesX = 5;
		this.lifesY = 5;
	}

	@Override
	public void render(float ptt) {

		if(this.transitioning) {
			ptt = (GameScreen.PORTAL_TRANSITION_TICKS - (GameScreen.PORTAL_TRANSITION_TICKS - this.transitionTicks))
					/ (float) GameScreen.PORTAL_TRANSITION_TICKS;
		}

		for(Entity e : this.currentWorld.getEntities()) {
			e.beforeRender(ptt);
		}

		this.camera.calcTranslation();

		GL11.glBegin(GL11.GL_QUADS);
		Color top = currentWorld.getBackgroundTop();
		Color bottom = currentWorld.getBackgroundBottom();
		top.bind();
		GL11.glVertex2f(0, 0);
		GL11.glVertex2f(Display.getWidth(), 0);
		bottom.bind();
		GL11.glVertex2f(Display.getWidth(), Display.getHeight());
		GL11.glVertex2f(0, Display.getHeight());
		GL11.glEnd();

		int blockSize = GameVariables.blockSize.getActualValue();
		TextureManager tm = this.game.getTextureManager();

		for(int x = 0; x < this.currentWorld.getWidth(); x++) {
			for(int y = 0; y < this.currentWorld.getHeight(); y++) {

				if(this.currentWorld.getBlock(x, y) > 0) {
					int x0 = -this.camera.gettX() + x * blockSize;
					int x1 = -this.camera.gettX() + x * blockSize + blockSize;
					int y0 = -this.camera.gettY() + y * blockSize;
					int y1 = -this.camera.gettY() + y * blockSize + blockSize;

					Block b = Block.getBlock(this.currentWorld.getBlock(x, y));
					int tB = this.currentWorld.getBlock(x, y - 1);
					int bB = this.currentWorld.getBlock(x, y + 1);
					int lB = this.currentWorld.getBlock(x - 1, y);
					int rB = this.currentWorld.getBlock(x + 1, y);

					int texID = b.getTexID();
					int sID = b.getSpritesheetID();

					if(b == Block.grass) {
						if(tB == 0 && bB == 0 && lB == 0 && rB != 0) {
							sID = 13;
						} else if(tB == 0 && bB == 0 && rB == 0 && lB != 0) {
							sID = 15;
						} else if(tB != 0 && bB == 0 && lB == 0 && rB != 0) {
							sID = 4;
						} else if(tB != 0 && bB == 0 && rB == 0 && lB != 0) {
							sID = 6;
						} else if(tB == 0 && bB != 0 && lB == 0 && rB != 0) {
							sID = 1;
						} else if(tB == 0 && bB != 0 && rB == 0 && lB != 0) {
							sID = 3;
						} else if(tB == 0 && bB != 0 && rB != 0 && lB != 0) {
							sID = 2;
						} else if(tB != 0 && bB == 0 && rB != 0 && lB != 0) {
							sID = 5;
						} else if(tB == 0 && bB == 0 && rB != 0 && lB != 0) {
							sID = 14;
						} else if(tB == 0 && bB == 0 && rB == 0 && lB == 0) {
							sID = 16;
						} else if(tB == 0 && bB != 0 && rB == 0 && lB == 0) {
							sID = 17;
						} else if(tB != 0 && bB != 0 && rB == 0 && lB == 0) {
							sID = 18;
						} else if(tB != 0 && bB != 0 && rB != 0 && lB == 0) {
							sID = 19;
						} else if(tB != 0 && bB != 0 && rB == 0 && lB != 0) {
							sID = 20;
						} else {
							sID = 7;
						}
					}

					if(b == Block.bouncy) {
						if(bB == 0) {
							sID = 23;
						}
					}

					GLUtil.drawFromAtlas(tm, texID, sID, x0, x1, y0, y1);
				}
			}
		}

		GL11.glPushMatrix();

		GL11.glTranslatef(-this.camera.gettX(), -this.camera.gettY(), 0);

		for(Entity e : this.currentWorld.getEntities()) {
			e.render(ptt, this.currentWorld);
		}

		for(Hint h : this.currentLevel.getHints()) {
			h.tryRender(ptt, this.currentWorld);
		}

		GL11.glPopMatrix();

		this.drawLifes();
		this.drawCoins();
	}

	@Override
	public void update() {
		if(this.transitioning) {

			if(this.transitionTicks == GameScreen.PORTAL_TRANSITION_TICKS / 2) {
				if(!this.isEnd) {
					Player p = this.currentWorld.getPlayer();

					this.currentWorldID++;
					if(this.currentWorldID > 1) {

						this.currentWorldID = 0;
					}

					this.currentWorld.unbindPlayer();
					p.nullVelocity();
					p.setLifes(p.getLifes() * 1.5f);
					this.currentWorld = this.currentLevel
							.getWorld(this.currentWorldID);
					this.currentWorld.bindPlayer(p);
				} else {
					this.currentLevelID++;
					if(this.currentLevelID == this.levels) {
						super.game.openScreen("diescreen");
						((DeadScreen) super.game.getScreen()).setWin();
						return;
					}

					this.currentLevel = loader.getLoadedLevels().get(
							currentLevelID);
					this.currentWorldID = 0;
					this.currentWorld = this.currentLevel.getWorld(0);
					this.camera = new Camera(this.currentWorld.getPlayer(), 1,
							0);
				}
			} else if(this.transitionTicks >= GameScreen.PORTAL_TRANSITION_TICKS) {
				this.transitioning = false;
			}

			this.transitionTicks++;

		}

		for(Block b : Block.getBlocks()) {
			if(b != null) {
				b.update(this.currentWorld);
			}
		}

		if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
			super.game.openScreen("mainmenu");
		}

		if(!this.transitioning) {
			this.currentWorld.update();
		}

		GameVariables.coins.setValue(GameVariables.coins.getActualValue()
				+ this.currentWorld.getPlayer().getCoinsFlush());

		if(this.currentWorld.getPlayer().getLifes() <= 0) {
			super.game.openScreen("diescreen");
			((DeadScreen) super.game.getScreen()).setDie();
		}

		if(!this.isTransitioning()) {
			this.handlePortal();
			this.handleExit();
		}
	}

	private void drawLifes() {
		TextureManager m = super.game.getTextureManager();
		GLUtil.drawTexture(m.getTexture(6), this.lifesX, this.lifesY);
		GLUtil.drawTextureCropped(m.getTexture(5), this.lifesX, this.lifesY,
				this.currentWorld.getPlayer().getLifes(), 1);
	}

	private void drawCoins() {
		TextureManager m = super.game.getTextureManager();
		GLUtil.drawFromAtlas(m, 2, 5, this.lifesX + 170, this.lifesY - 5);
		super.game.getFontManager().drawString(
				GameVariables.coins.getActualValue() + "", this.lifesX + 200,
				this.lifesY - 8, Color.white);
	}

	private void handlePortal() {
		if(this.currentWorld.getPlayer().getStandingOnID() == Block.portal
				.getID()) {

			this.transitionTicks = 0;
			this.transitioning = true;
			this.isEnd = false;

			this.game.getSoundManager().playSound("portal");
		}

	}

	private void handleExit() {
		if(this.currentWorld.getPlayer().getStandingOnID() == Block.exit
				.getID()) {
			this.isEnd = true;
			this.transitioning = true;

			this.transitionTicks = 0;
			this.game.getSoundManager().playSound("portal");
		}
	}

	@Override
	public void onClosing() {
	}

	public boolean isTransitioning() {
		return transitioning;
	}

	public int getTransitionTicks() {
		return transitionTicks;
	}

}
