package cz.dat.ld30.screen;

import cz.dat.ld30.Game;

public abstract class Screen {
	protected Game game;
	protected String title;
	private boolean wasOpened;
	
	public Screen(String title, Game game) {
		this.game = game;
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void onOpening() {
		this.wasOpened = true;
	}
	
	public boolean wasOpened() {
		return this.wasOpened;
	}
	
	public abstract void render(float ptt);
	public abstract void update();
	public abstract void onClosing();
}
