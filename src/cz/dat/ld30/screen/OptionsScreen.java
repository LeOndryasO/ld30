package cz.dat.ld30.screen;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;

import cz.dat.ld30.FontManager;
import cz.dat.ld30.Game;
import cz.dat.ld30.GameVariables;
import cz.dat.ld30.util.Coord2D;
import cz.dat.ld30.util.CoordUtil;
import cz.dat.ld30.util.GLUtil;

public class OptionsScreen extends Screen {

	private Coord2D[] resolutions;
	private Coord2D currentResolution;
	private int currentResolutionSelected = 0;
	
	private int[] blockSizes;
	private int currentBlockSizeSelected;
	
	private int lineSelected;
	private int lines = 3;
	
	TrueTypeFont f;
	
	private String restartString = "You must restart the game for the changes to take effect";

	public OptionsScreen(Game game) {
		super("Options", game);
		this.initLines();
		this.f = super.game.getFontManager().getSized(FontManager.FONT_BIG);
	}

	private void initLines() {
		this.resolutions = new Coord2D[] { new Coord2D(640, 480),
				new Coord2D(800, 480), new Coord2D(854, 480),
				new Coord2D(1024, 768), new Coord2D(1280, 720),
				new Coord2D(1600, 900) };
		this.currentResolution = new Coord2D(
				GameVariables.width.getActualValue(),
				GameVariables.height.getActualValue());

		for(int i = 0; i < this.resolutions.length; i++) {
			if(this.resolutions[i].equals(this.currentResolution)) {
				this.currentResolutionSelected = i;
				break;
			}
		}
		
		this.blockSizes = new int[] { 8, 16, 32, 64 };
		for(int i = 0; i < this.blockSizes.length; i++) {
			if(this.blockSizes[i] == GameVariables.blockSize.getActualValue()) {
				this.currentBlockSizeSelected = i;
				break;
			}
		}
	}

	private String c2dAsRes(Coord2D c) {
		return c.x + "x" + c.y;
	}

	private void renderResolution() {
		String res = "Resolution: " + this.c2dAsRes(this.currentResolution);
		int x = CoordUtil.getXCentered(GameVariables.width.getFirstValue() / 2,
				super.game.getFontManager().getSized(FontManager.FONT_BIG).getWidth(res));

		super.game.getFontManager().drawString(res, x, 30, FontManager.FONT_BIG, Color.white);
		
		if(this.lineSelected == 0) {
			GLUtil.drawLine(x - 5, x + f.getWidth(res) + 5, f.getHeight() + 30, 
					f.getHeight() + 30, 3, 0.5f, 0.75f, 0.69f, 0.75f);
		}
	}

	private void renderBlockSize() {
		String res = "Block size: " + GameVariables.blockSize.getActualValue();
		int x = CoordUtil.getXCentered(GameVariables.width.getFirstValue() / 2,
				f.getWidth(res));

		super.game.getFontManager().drawString(res, x, 30 + f.getHeight(), FontManager.FONT_BIG, Color.white);
		
		if(this.lineSelected == 1) {
			GLUtil.drawLine(x - 5, x + f.getWidth(res) + 5, f.getHeight() * 2 + 30, 
					f.getHeight() * 2 + 30, 3, 0.5f, 0.75f, 0.69f, 0.75f);
		}
	}
	
	private void renderSound() {
		String res = (GameVariables.soundOn.getActualValue() == 1) ? "Sound ON" : "Sound OFF";
		int x = CoordUtil.getXCentered(GameVariables.width.getFirstValue() / 2,
				f.getWidth(res));
		
		super.game.getFontManager().drawString(res, x, 30 + f.getHeight() * 2, FontManager.FONT_BIG, Color.white);
		
		if(this.lineSelected == 2) {
			GLUtil.drawLine(x - 5, x + f.getWidth(res) + 5, f.getHeight() * 3 + 30, 
					f.getHeight() * 3 + 30, 3, 0.5f, 0.75f, 0.69f, 0.75f);
		}
	}
	
	private void resUp() {
		this.currentResolutionSelected++;
		if(this.currentResolutionSelected == this.resolutions.length) {
			this.currentResolutionSelected = 0;
		}

		this.currentResolution = this.resolutions[this.currentResolutionSelected];
	}

	private void resDown() {
		this.currentResolutionSelected--;
		if(this.currentResolutionSelected < 0) {
			this.currentResolutionSelected = (this.resolutions.length - 1);
		}

		this.currentResolution = this.resolutions[this.currentResolutionSelected];
	}

	private void bsUp() {
		this.currentBlockSizeSelected++;
		if(this.currentBlockSizeSelected == this.blockSizes.length) {
			this.currentBlockSizeSelected = 0;
		}
		
		GameVariables.blockSize.setValue(this.blockSizes[this.currentBlockSizeSelected]);
	}
	
	private void bsDown() {
		this.currentBlockSizeSelected--;
		if(this.currentBlockSizeSelected < 0) {
			this.currentBlockSizeSelected = this.blockSizes.length - 1;
		}
		
		GameVariables.blockSize.setValue(this.blockSizes[this.currentBlockSizeSelected]);
	}
	
	private void setSound() {
		GameVariables.soundOn.setValue(
				(GameVariables.soundOn.getActualValue() == 1) ? 0 : 1);
		super.game.getSoundManager().updateVolume((GameVariables.soundOn.getActualValue() == 1), 1);
	}
	
	@Override
	public void render(float ptt) {
		this.renderResolution();
		this.renderBlockSize();
		this.renderSound();

		int x = CoordUtil.getXCentered(
				GameVariables.width.getFirstValue() / 2,
				super.game.getFontManager().getFont()
						.getWidth(this.restartString));

		super.game.getFontManager().drawString(
				this.restartString,
				x,
				GameVariables.height.getFirstValue()
						- super.game.getFontManager().getFont().getHeight()
						- 10, Color.white);
	}


	@Override
	public void update() {
		while(Keyboard.next()) {
			if(Keyboard.getEventKeyState()) {
				int key = Keyboard.getEventKey();

				if(key == Keyboard.KEY_ESCAPE) {
					super.game.openScreen("mainmenu");
					super.game.getSoundManager().playSound("menugo", 1.5f);
					
					return;
				}
				
				if(key == Keyboard.KEY_DOWN) {
					this.lineSelected++;
					if(this.lineSelected == this.lines) {
						this.lineSelected = 0;
					}
				}
				
				if(key == Keyboard.KEY_UP) {
					this.lineSelected--;
					if(this.lineSelected < 0) {
						this.lineSelected = this.lines - 1;
					}
				}

				if(key == Keyboard.KEY_RIGHT) {
					switch(this.lineSelected) {
					case 0:
						this.resUp();
						break;
					case 1:
						this.bsUp();
						break;
					case 2:
						this.setSound();
						break;
					}
					
					super.game.getSoundManager().playSound("menu");
				}

				if(key == Keyboard.KEY_LEFT) {
					switch(this.lineSelected) {
					case 0:
						this.resDown();
						break;
					case 1:
						this.bsDown();
						break;
					case 2:
						this.setSound();
						break;
					}
					
					
					super.game.getSoundManager().playSound("menu");
				}
			}
		}
	}
	
	@Override
	public void onClosing() {
		GameVariables.width.setValue(this.currentResolution.x);
		GameVariables.height.setValue(this.currentResolution.y);
	}

}
