package cz.dat.ld30.screen;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.opengl.Texture;

import cz.dat.ld30.FontManager;
import cz.dat.ld30.Game;
import cz.dat.ld30.GameVariables;
import cz.dat.ld30.TextureManager;
import cz.dat.ld30.util.CoordUtil;
import cz.dat.ld30.util.GLUtil;

public class MainMenuScreen extends Screen {

	private TrueTypeFont fontLg;
	
	private String[] menuLines;
	private int menuLinesHeight;
	private Texture logo;
	private int selectedLine = 0;
	private int coinsX;
	private int coinsY;
	
	public MainMenuScreen(Game game) {
		super("Menu", game);
		
		this.fontLg = game.getFontManager().getSized(FontManager.FONT_BIG);
		this.logo = super.game.getTextureManager().getTexture(0);
		
		this.initLines();
	}
	
	private void initLines() {
		this.menuLines = new String[] { "Play game", "Reset game", "Options", "About", "Exit" };
		this.menuLinesHeight = this.menuLines.length * this.fontLg.getHeight();
	}
	
	private void handleEnter() {
		switch(this.selectedLine) {
		case 0:
			super.game.openScreen("game");
			return;
		case 1:
			super.game.resetGameScreen();
			super.game.openScreen("game");
			return;
		case 2:
			super.game.openScreen("options");
			return;
		case 3:
			super.game.openScreen("about");
			return;
		case 4:
			super.game.getSoundManager().playSound("menugo");
			
			try {
				Thread.sleep(800);
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
			
			super.game.exit();
			return;
		}
	}

	@Override
	public void render(float ptt) {
		GLUtil.drawTexture(super.game.getTextureManager().getTexture(0),
				((GameVariables.width.getFirstValue() / 2) - (this.logo.getImageWidth() / 2)), 20);
		
		int y = ((GameVariables.height.getFirstValue() / 2) - (this.menuLinesHeight / 2));
		for(int i = 0; i < this.menuLines.length; i++) {
			int x = CoordUtil.getXCentered(GameVariables.width.getFirstValue() / 2, 
					this.fontLg.getWidth(this.menuLines[i]));
			
			GL11.glEnable(GL11.GL_TEXTURE_2D);
			this.fontLg.drawString(x, y, this.menuLines[i], Color.white);
			GL11.glDisable(GL11.GL_TEXTURE_2D);
			
			y += this.fontLg.getHeight();
			
			if(i == this.selectedLine) {
				GLUtil.drawLine(x - 5, x + this.fontLg.getWidth(this.menuLines[i]) + 5, y - 5, y - 5, 3, 0.5f, 0.75f, 0.69f, 0.75f);
			}
		}
		
		this.drawCoins();
	}

	private void drawCoins() {
		TextureManager m = super.game.getTextureManager();
		GLUtil.drawFromAtlas(m, 2, 5, this.coinsX, this.coinsY);
		super.game.getFontManager().drawString(GameVariables.coins.getActualValue() + "", 
				this.coinsX + 30, this.coinsY - 3, Color.white);
	}
	
	@Override
	public void update() {
		while(Keyboard.next()) {
			if(Keyboard.getEventKeyState()) {
				int key = Keyboard.getEventKey();
				
				if(key == Keyboard.KEY_DOWN) {
					this.selectedLine++;
					if(this.selectedLine == 1) {
						if(!super.game.getScreen("game").wasOpened()) {
							this.selectedLine = 2;
						}
					}
					
					if(this.selectedLine > (this.menuLines.length - 1)) {
						this.selectedLine = 0;
					}
					
					super.game.getSoundManager().playSound("menu");
				}
				
				if(key == Keyboard.KEY_UP) {
					this.selectedLine--;
					if(this.selectedLine == 1) {
						if(!super.game.getScreen("game").wasOpened()) {
							this.selectedLine = 0;
						}
					}
					
					if(this.selectedLine < 0) {
						this.selectedLine = (this.menuLines.length - 1);
					}
					
					super.game.getSoundManager().playSound("menu");
				}
				
				if(key == Keyboard.KEY_RETURN || key == Keyboard.KEY_SPACE) {
					this.handleEnter();
					
					super.game.getSoundManager().playSound("menugo");
				}
			}
		}	
	}

	@Override
	public void onOpening() {
		super.onOpening();
		this.selectedLine = 0;
		Keyboard.destroy();
		
		try {
			Keyboard.create();
		} catch(LWJGLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onClosing() {
	}

}
