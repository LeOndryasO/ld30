package cz.dat.ld30.screen;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;

import cz.dat.ld30.FontManager;
import cz.dat.ld30.Game;
import cz.dat.ld30.GameVariables;
import cz.dat.ld30.util.CoordUtil;
import cz.dat.ld30.util.GLUtil;

public class AboutScreen extends Screen {

	private String[] strings;
	private int height;
	private FontManager f;
	
	public AboutScreen(Game game) {
		super("About the game", game);
		this.f = game.getFontManager();
		
		this.strings = new String[] { "Game developed for Ludum Dare 30 Jam", "By Dax105 and LeOndryasO", "https://bitbucket.org/LeOndryasO/ld30"
				, "Most of the textures by Lars Doucet, Sean Choate", " and Megan Bednarz - http://bit.ly/VKBqFh"
				, "Music from this awesome YT channel: ", "https://www.youtube.com/user/teknoaxe", "http://www.ludumdare.com/", "http://www.ondryaso.eu/"
				, "Made in the Czech Republic" };
		
		this.height = (this.strings.length * (this.f.getFont().getHeight() - 5));
	}

	@Override
	public void render(float ptt) {
		Texture t = super.game.getTextureManager().getTexture(10);
		
		int y = ((GameVariables.height.getFirstValue() / 2) - (this.height / 2));
		
		for(int i = 0; i < this.strings.length; i++) {
			int x = CoordUtil.getXCentered(GameVariables.width.getFirstValue() / 2, 
					this.f.getFont().getWidth(this.strings[i]));
			
			this.f.drawString(this.strings[i], x, y, Color.white);
			
			y += this.f.getFont().getHeight() - 5;
		}
		
		y += 5;
		
		GLUtil.drawTexture(t, (GameVariables.width.getFirstValue() / 2) - (t.getImageHeight() / 2), y);
	}

	@Override
	public void update() {
		while(Keyboard.next()) {
			if(Keyboard.getEventKeyState()) {
				if(Keyboard.getEventKey() == Keyboard.KEY_ESCAPE) {
					super.game.openScreen("mainmenu");
				}
			}
		}
	}

	@Override
	public void onClosing() {
	}

}
