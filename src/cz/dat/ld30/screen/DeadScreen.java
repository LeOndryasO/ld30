package cz.dat.ld30.screen;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.TrueTypeFont;

import cz.dat.ld30.FontManager;
import cz.dat.ld30.Game;
import cz.dat.ld30.GameVariables;

public class DeadScreen extends Screen {

	private TrueTypeFont fontLg;
	private TrueTypeFont fontSm;
	private FontManager fontManager;
	private String deadString = "You are dead :(";
	private String menuString = "Return to menu";
	private int linesHeight;
	private long returnTimer = 0;

	public DeadScreen(Game game) {
		super("Death", game);
		this.fontManager = game.getFontManager();
		this.fontLg = game.getFontManager().getSized(FontManager.FONT_BIG);
		this.fontSm = game.getFontManager().getFont();
		this.linesHeight = this.fontLg.getHeight(this.deadString)
				+ this.fontManager.getFont().getHeight();
	}

	@Override
	public void render(float ptt) {
		int x = (GameVariables.width.getFirstValue() / 2)
				- (this.fontLg.getWidth(this.deadString) / 2);

		GL11.glEnable(GL11.GL_TEXTURE_2D);
		this.fontLg.drawString(x, (GameVariables.height.getFirstValue() / 2)
				- (this.linesHeight / 2), this.deadString);
		if(this.returnTimer > 40) {
			this.fontSm.drawString(
					x,
					(GameVariables.height.getFirstValue() / 2)
							- (this.linesHeight / 2)
							+ this.fontLg.getHeight(this.menuString),
					this.menuString);
		}
		GL11.glDisable(GL11.GL_TEXTURE_2D);
	}

	@Override
	public void update() {
		this.returnTimer++;

		if((Keyboard.isKeyDown(Keyboard.KEY_RETURN) || Keyboard
				.isKeyDown(Keyboard.KEY_SPACE)) && this.returnTimer > 40) {
			super.game.openScreen("mainmenu");
		}
	}

	public void setWin() {
		this.deadString = "You won and got "
				+ (GameVariables.coins.getActualValue() - GameVariables.coins
						.getFirstValue()) + " coins! :)";
		this.title = "Win";
		this.linesHeight = this.fontLg.getHeight(this.deadString)
				+ this.fontManager.getFont().getHeight();
	}

	public void setDie() {
		this.deadString = "You are dead :(";
		this.title = "Death";
		this.linesHeight = this.fontLg.getHeight(this.deadString)
				+ this.fontManager.getFont().getHeight();
	}

	@Override
	public void onOpening() {
		super.onOpening();
		super.game.resetGameScreen();
	}

	@Override
	public void onClosing() {

	}

}
