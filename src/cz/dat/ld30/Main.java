package cz.dat.ld30;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Logger;

public class Main {

	private static Logger logger;
	private static Game currentInstance;
	public static File conf = new File("config.dat");

	public static Game getInstance() {
		return Main.currentInstance;
	}

	public static void main(String[] args) {
		Main.logger = Logger.getLogger("ld30gamelogger");

		if(!conf.exists()) {
			try {
				conf.createNewFile();
			} catch(IOException e) {
				e.printStackTrace();
			}
		} else {
			try {
				GameVariables.load(conf);
			} catch(FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		Game game = new Game();
		Main.currentInstance = game;

		Thread t = new Thread(game);
		t.setName("Game thread");
		t.start();
	}

	public static Logger getLogger() {
		return Main.logger;
	}

}
