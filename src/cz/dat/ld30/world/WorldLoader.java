package cz.dat.ld30.world;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.imageio.ImageIO;

import org.newdawn.slick.Color;
import org.newdawn.slick.util.ResourceLoader;

import cz.dat.ld30.Game;
import cz.dat.ld30.GameVariables;
import cz.dat.ld30.entity.FollowingEnemy;
import cz.dat.ld30.entity.WalkingEnemy;

public class WorldLoader {
	private final String WORLDS_PATH = GameVariables.resFolder + "level/";
	private final String LEVEL_PLACEHOLDER = "l%Lw%W.png";
	private final String LEVEL_HINTS_PLACEHOLDER = "l%Lht.dat";
	private final String LEVEL_REGEXP = "l[0-9]+w[0-9]+\\.png";

	private Game game;
	private List<Level> loaded;

	private Random rnd;

	public WorldLoader(Game game) {
		this.game = game;
		this.rnd = new Random();
	}

	public List<Level> getLoadedLevels() {
		return this.loaded;
	}

	public void loadLevels() {
		final File jarFile = new File(getClass().getProtectionDomain()
				.getCodeSource().getLocation().getPath());

		this.loaded = new ArrayList<Level>();
		List<String> names = this.getNames();
		int levelCount = 0;

		for(String f : names) {
			if(f.endsWith(".png")) {
				String name = f
						.substring(f.lastIndexOf(jarFile.isFile() ? "/"
								: File.separator) + 1, f.length());
				if(!name.matches(this.LEVEL_REGEXP)) {
					continue;
				}

				int level = Integer.parseInt(name.substring(1,
						name.indexOf('w')));
				if(level > levelCount) {
					levelCount = level;
				}
			}
		}

		if((levelCount * 2) > names.size()) {
			return;
		}

		for(int i = 1; i <= levelCount; i++) {
			System.out.println("Loading level " + i);
			this.loadLevel(i, false);
		}
	}

	public List<String> getNames() {
		List<String> ret = new ArrayList<String>();

		final File jarFile = new File(getClass().getProtectionDomain()
				.getCodeSource().getLocation().getPath());

		if(jarFile.isFile()) { // Run with JAR file
			JarFile jar;
			try {
				jar = new JarFile(jarFile);
				final Enumeration<JarEntry> entries = jar.entries();
				while(entries.hasMoreElements()) {
					final String name = entries.nextElement().getName();
					if(name.startsWith(this.WORLDS_PATH)) {
						ret.add(name);
					}
				}

				jar.close();
			} catch(IOException e) {
				e.printStackTrace();
			}

		} else { // Run with IDE
			final URL url = ResourceLoader.getResource(this.WORLDS_PATH);
			if(url != null) {
				try {
					final File apps = new File(url.toURI());
					for(File app : apps.listFiles()) {
						ret.add(app.getAbsolutePath());
					}
				} catch(URISyntaxException ex) {
					// never happens
				}
			}
		}

		return ret;
	}

	public Level loadLevel(int id, boolean load) {
		if(load) {
			for(Level l : this.loaded) {
				if(l.getLevelID() == id) {
					return l;
				}
			}
		}

		Level level = new Level(id);

		try {
			Color[] comb = this.getColorCombination(null);

			level.setWorld(
					0,
					this.loadWorld(
							WORLDS_PATH
									+ LEVEL_PLACEHOLDER.replace("%L", "" + id)
											.replace("%W", "" + 1))
							.setBackgroundBottom(comb[0])
							.setBackgroundTop(comb[1]));

			comb = this.getColorCombination(comb);

			level.setWorld(
					1,
					this.loadWorld(
							WORLDS_PATH
									+ LEVEL_PLACEHOLDER.replace("%L", "" + id)
											.replace("%W", "" + 2))
							.setBackgroundBottom(comb[0])
							.setBackgroundTop(comb[1]));
		} catch(IOException e) {
			e.printStackTrace();
		}

		this.loaded.add(level);

		this.loadHints(level);
		return level;
	}

	public World loadWorld(String path) throws IOException {
		if(!(new File(path).isDirectory())) {

			BufferedImage img = ImageIO.read(ResourceLoader
					.getResourceAsStream(path));
			int w = img.getWidth();
			int h = img.getHeight();

			World newWorld = new World(this.game, w, h);

			for(int y = 0; y < h; y++) {
				for(int x = 0; x < w; x++) {
					int color = img.getRGB(x, y);

					int blockID = Block.getBlockForColor(color);
					if(blockID == -1) {
						this.createEntity(newWorld, x, y, color);
					} else {
						newWorld.setBlock(x, y, blockID);
					}
				}
			}

			return newWorld;
		} else {
			return null;
		}
	}

	public void loadHints(Level level) {
		String path = this.WORLDS_PATH
				+ this.LEVEL_HINTS_PLACEHOLDER.replace("%L", level.getLevelID()
						+ "");
		Scanner s = new Scanner(ResourceLoader.getResourceAsStream(path));

		while(s.hasNextLine()) {
			String l = s.nextLine();
			String[] parts = l.split(";");
			int fromX = Integer.parseInt(parts[0]);
			int toX = Integer.parseInt(parts[1]);
			int world = Integer.parseInt(parts[2]);

			Hint h = ((world == -1) ? new Hint(parts[3], fromX, toX, level)
					: new Hint(parts[3], fromX, toX, level, world));
			level.getHints().add(h);
		}

		s.close();

	}

	public void createEntity(World world, int x, int y, int color) {
		switch (color){
		case 0xFF123456:
			world.addEntity(new FollowingEnemy(x, y));
			break;
		case 0xFF654321:
			world.addEntity(new WalkingEnemy(x, y));
			break;
		case 0xFFFF0000:
			world.createPlayer(x, y);
			break;
		}
	}

	Color[][] combs = new Color[][] {
			{ new Color(140, 28, 28), new Color(255, 187, 0) },
			{ new Color(0, 143, 245), new Color(143, 216, 255) },
			{ new Color(0, 153, 17), new Color(177, 247, 0) },
			{ new Color(219, 190, 0), new Color(255, 238, 0) } };

	private Color[] getColorCombination(Color[] not) {
		Color[] r = this.combs[rnd.nextInt(combs.length)];

		if(r.equals(not)) {
			return this.getColorCombination(not);
		}

		return r;
	}
}
