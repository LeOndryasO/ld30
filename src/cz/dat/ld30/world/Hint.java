package cz.dat.ld30.world;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;

import cz.dat.ld30.GameVariables;
import cz.dat.ld30.util.GLUtil;

public class Hint {
	
	private boolean bothWorlds = false;
	private int fromX, toX;
	private String text;
	private int wID;
	private Level level;
	private TrueTypeFont font;
	private int textWidth;
	
	private Color boundsColor = new Color(0.25f, 0.25f, 0.25f, 1);
	private Color innerColor = new Color(0.5f, 0.5f, 0.5f, 0.8f);

	public Hint(String text, int fromX, int toX, Level level) {
		this(text, fromX, toX, level, -1);
		this.bothWorlds = true;
	}

	public Hint(String text, int fromX, int toX, Level level, int worldID) {
		this.fromX = fromX;
		this.toX = toX;
		this.wID = worldID;
		this.level = level;
		this.font = level.getWorld(0).getGame().getFontManager().getFont();
		this.setText(text);
	}
	
	public void setWorld(int id) {
		if(id == -1) {
			this.bothWorlds = true;
		} else {
			this.bothWorlds = false;
			this.wID = id;
		}
	}

	public void setText(String text) {
		this.text = text;
		this.textWidth = this.font.getWidth(text);
	}
	
	public void tryRender(float ptt, World world) {
		if(this.bothWorlds || this.level.getWorld(this.wID) == world) {
			if(world.getPlayer().getBB().x0 >= this.fromX
					&& world.getPlayer().getBB().x1 <= this.toX) {
				
				int bs = GameVariables.blockSize.getActualValue();
				
				int boxWidth = this.textWidth + 10;
				int boxHeight = this.font.getHeight() + 6;
				int boxX = (int) ((world.getPlayer().getRenderBB().x1+1)*bs);
				int boxY = (int) ((world.getPlayer().getRenderBB().y0+1)*bs);
				
				this.renderInner(boxX, boxX + boxWidth, boxY, boxY + boxHeight, this.innerColor);
				this.renderBounds(boxX, boxX + boxWidth, boxY, boxY + boxHeight, this.boundsColor);
				
				GL11.glEnable(GL11.GL_TEXTURE_2D);
				this.font.drawString(boxX + 5, boxY, this.text);
				GL11.glDisable(GL11.GL_TEXTURE_2D);
				
			}
		}
	}
	
	private void renderBounds(int x1, int x2, int y1, int y2, Color color) {
		GLUtil.drawLine(x1, x2, y1, y1, 2);
		GLUtil.drawLine(x1, x1, y1, y2, 2);
		GLUtil.drawLine(x2, x2, y1, y2, 2);
		GLUtil.drawLine(x1, x2, y2, y2, 2);
	}
	
	private void renderInner(int x1, int x2, int y1, int y2, Color color) {	
		GLUtil.drawRectangle(color, x1, x2, y1, y2);
	}
}
