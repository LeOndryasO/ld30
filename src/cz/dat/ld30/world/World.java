package cz.dat.ld30.world;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.newdawn.slick.Color;

import cz.dat.ld30.Game;
import cz.dat.ld30.entity.Entity;
import cz.dat.ld30.entity.Player;

public class World {
	
	private byte[][] blocks;
	
	private Color backgroundBottom = new Color(0, 143, 245);
	private Color backgroundTop = new Color(143, 216, 255);
	private Game game;
	private int width;
	private int height;
	private Player player;
	private List<Entity> entities;
	private List<Entity> addEntities;
	private List<Entity> removeEntities;
	
	public World(Game game, int width, int height) {
		this.game = game;
		this.width = width;
		this.height = height;
		
		this.entities = new LinkedList<Entity>();
		this.addEntities = new LinkedList<Entity>();
		this.removeEntities = new LinkedList<Entity>();
		
		this.blocks = new byte[width][height];
	}
		
	public int getBlock(int x, int y) {
		if (x >= 0 && x < this.width && y >= 0 && y < this.height) {
			return blocks[x][y];
		}
		return 0;
	}
	
	public void setBlock(int x, int y, int id) {
		if (x >= 0 && x < this.width && y >= 0 && y < this.height) {
			blocks[x][y] = (byte) id;
		}
	}
	
	public void addEntity(Entity e) {
		this.addEntities.add(e);
	}
	
	public void removeEntity(Entity e) {
		this.removeEntities.add(e);
	}
	
	public List<Entity> getEntities() {
		return this.entities;
	}
	
	public void update() {		
		for(Entity e : this.entities) {
			e.update(this);
		}
		
		for(Iterator<Entity> it = this.addEntities.iterator(); it.hasNext();) {
			Entity e = it.next();
			this.entities.add(e);
			it.remove();
		}
		
		for(Iterator<Entity> it = this.removeEntities.iterator(); it.hasNext();) {
			Entity e = it.next();
			this.entities.remove(e);
			it.remove();
		}
	}

	
	public void createPlayer(int x, int y) {
		this.player = new Player(x, y);
		this.entities.add(this.player);
	}
	
	public void bindPlayer(Player p) {
		this.player = p;
		this.entities.add(this.player);
	}

	public void unbindPlayer() {
		this.entities.remove(this.player);
		this.player = null;
	}
	
	public Game getGame() {
		return game;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	public Color getBackgroundBottom() {
		return backgroundBottom;
	}

	public World setBackgroundBottom(Color backgroundBottom) {
		this.backgroundBottom = backgroundBottom;
		return this;
	}

	public Color getBackgroundTop() {
		return backgroundTop;
	}

	public World setBackgroundTop(Color backgroundTop) {
		this.backgroundTop = backgroundTop;
		return this;
	}
	
	public Player getPlayer() {
		return this.player;
	}

	
}
