package cz.dat.ld30.world;

public class AnimatedBlock extends Block {

	private int inSheet;
	private int ticks = 0;
	private int ticksForChange = 10;
	
	public AnimatedBlock(int id, int imagesInSheet) {
		super(id);
		this.inSheet = imagesInSheet;
	}

	public AnimatedBlock setTicksForChange(int t) {
		this.ticksForChange = t;
		return this;
	}
	
	public void update(World world) {
		ticks++;
		if(ticks >= this.ticksForChange) {
			ticks = 0;
			
			this.ssID++;
			if(this.ssID > this.inSheet) {
				this.ssID = 1;
			}
		}
	}
}
