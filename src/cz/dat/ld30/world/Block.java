package cz.dat.ld30.world;

public class Block {

	public static final Block grass = new Block(1).setTexID(1).setSpritesheetID(7).setSaveColor(0xFF000000);
	public static final Block coin = new AnimatedBlock(2, 8).setTicksForChange(5).setTexID(2).setCollidable(false).setSaveColor(0xFFABCDEF);
	public static final Block bouncy = new Block(3).setTexID(1).setSpritesheetID(9).setSaveColor(0xFF00FF00);
	public static final Block help = new Block(4).setTexID(1).setSpritesheetID(10).setCollidable(false).setSaveColor(0xFFFFFF00);
	public static final Block portal = new AnimatedBlock(5, 4).setTexID(3).setCollidable(false).setSaveColor(0xFF0000FF);
	public static final Block exit = new Block(6).setTexID(1).setSpritesheetID(8).setCollidable(false).setSaveColor(0xFFFF00FF);
	public static final Block ice = new Block(7).setTexID(1).setSpritesheetID(11).setSaveColor(0xFF00FFD2);
	public static final Block tnt = new Block(8).setTexID(1).setSpritesheetID(12).setCollidable(false).setSaveColor(0xFFFF8000);
	public static final Block brick = new Block(9).setTexID(1).setSpritesheetID(21).setSaveColor(0xFFFF9A00);
	public static final Block stone = new Block(10).setTexID(1).setSpritesheetID(22).setSaveColor(0xFF4C4C4C);
	
	
	private static Block[] blocks;
	private static int[] blockColors;

	public static void registerBlock(int id, Block b) {
		if(Block.blocks == null) {
			Block.blocks = new Block[64];
		}
		
		Block.blocks[id] = b;
	}
	
	public static Block getBlock(int id) {
		return Block.blocks[id];
	}
	
	public static int getBlockForColor(int color) {
		for(int i = 0; i < Block.blockColors.length; i++) {
			if(Block.blockColors[i] == color) {
				return i;
			}
		}
		
		return -1;
	}
	
	public static Block[] getBlocks() {
		return Block.blocks;
	}
	
	protected int id;
	protected int texID = 0;
	protected int saveColor = 0x00000000;
	protected int ssID = 1;
	protected boolean collidable = true;
	
	public Block(int id) {
		Block.registerBlock(id, this);
		this.id = id;
	}
	
	public Block setTexID(int id) {
		this.texID = id;
		return this;
	}
	
	public Block setCollidable(boolean collidable) {
		this.collidable = collidable;
		return this;
	}
	
	public Block setSpritesheetID(int id) {
		this.ssID = id;
		return this;
	}
	
	public int getID() {
		return id;
	}

	public int getTexID() {
		return texID;
	}
	
	public int getSpritesheetID() {
		return this.ssID;
	}

	public boolean isCollidable() {
		return collidable;
	}

	public int getSaveColor() {
		return saveColor;
	}

	public Block setSaveColor(int saveColor) {
		this.saveColor = saveColor;
		
		if(Block.blockColors == null) {
			Block.blockColors = new int[64];
		}
		
		Block.blockColors[this.id] = saveColor;
		return this;
	}
	
	public void update(World world) {
	}
	
}
