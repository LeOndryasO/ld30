package cz.dat.ld30.world;

import java.util.ArrayList;
import java.util.List;

public class Level {
	private World[] worlds = new World[2];
	private int levelID;
	private List<Hint> hints = new ArrayList<Hint>();
	
	public Level(int levelID) {
		this.levelID = levelID;
	}
	
	public int getLevelID() {
		return this.levelID;
	}
	
	public World getWorld(int id) {
		return this.worlds[id];
	}
	
	public void setWorld(int id, World w) {
		this.worlds[id] = w;
	}
	
	public List<Hint> getHints() {
		return this.hints;
	}
	
	public int hashCode() {
		return (this.levelID * 13 + (this.worlds[0] == null ? 1 : this.worlds[0].hashCode())
				+ (this.worlds[1] == null ? 1 : this.worlds[1].hashCode()));
	}
	
	public boolean equals(Object other) {
		if(other instanceof Level) {
			Level l = (Level) other;
			return (l.levelID == this.levelID && l.getWorld(0) == this.getWorld(0) && l.getWorld(1) == this.getWorld(1));
		}
		
		return false;
	}
}
