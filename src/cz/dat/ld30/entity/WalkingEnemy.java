package cz.dat.ld30.entity;

import org.lwjgl.opengl.GL11;

import cz.dat.ld30.GameVariables;
import cz.dat.ld30.TextureManager;
import cz.dat.ld30.util.AABB;
import cz.dat.ld30.util.GLUtil;
import cz.dat.ld30.world.World;

public class WalkingEnemy extends Entity {
	public static final float SIZE = 1.0f;

	private float speed = 0.05f;
	private int hurtTimer = 5;
	private boolean facingLeft = false;

	public WalkingEnemy(int posX, int posY) {
		super(new AABB(posX, posY-1, posX + SIZE, posY-1 + SIZE));
	}

	private float velX = 0;
	private float velY = 0;

	private int timer = 0;
	private int tex = 1;
	
	@Override
	public void update(World world) {
		super.lastBB = this.bb.clone();

		timer++;
		
		if (timer >= 2) {
			tex++;
			if (tex > 2) {
				tex = 1;
			}
			this.timer = 0;
		}
		
		Player p = world.getPlayer();

		if(this.facingLeft) {
			this.velX -= this.speed;
		} else {
			this.velX += this.speed;
		}

		if(p.getBB().intersects(this.bb)) {
			hurtTimer++;

			if(hurtTimer >= 5) {
				p.hurt(30);
				world.getGame().getSoundManager().playSound("hit");
				hurtTimer = 0;
			}
		}

		this.velX *= 0.8f;

		this.velY += 0.1f;
		this.velY *= 0.8f;

		float[] clipped = this.bb.moveCollide(world, this.velX, this.velY);

		if(clipped[0] != this.velX) {
			this.velX = 0;
			this.facingLeft = !this.facingLeft;
		}
		
		if(clipped[1] != this.velY) {
			this.velY = 0;
		}

	}

	@Override
	public void beforeRender(float ptt) {
		super.renderBB = AABB.mix(this.lastBB, this.bb, ptt);
	}

	@Override
	public void render(float ptt, World world) {
		int bs = GameVariables.blockSize.getActualValue();

		GL11.glPushMatrix();
		GL11.glTranslatef(SIZE * bs, 0, 0);

		TextureManager tm = world.getGame().getTextureManager();
		
		GLUtil.drawFromAtlas(tm, 9, this.tex,
				super.renderBB.x0 * bs,
				super.renderBB.x1 * bs,
				super.renderBB.y0 * bs + bs,
				super.renderBB.y1 * bs + bs);

		GL11.glPopMatrix();
	}

}
