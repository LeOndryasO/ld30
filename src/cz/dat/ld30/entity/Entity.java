package cz.dat.ld30.entity;

import cz.dat.ld30.util.AABB;
import cz.dat.ld30.world.World;


public abstract class Entity {
	protected AABB bb;
	protected AABB lastBB;
	protected AABB renderBB;
	protected boolean isColliding = true;
	protected float lifes = 1;
	
	public Entity(AABB bb) {
		this.bb = bb;
		this.lastBB = this.bb;
		this.renderBB = this.bb;
	}
	
	public Entity(int x, int y, int width, int height) {
		this(new AABB(x, y, x+width, y+height));
	}
	
	public boolean canCollide() {
		return this.isColliding;
	}
	
	public abstract void beforeRender(float ptt);
	
	public void setColliding(boolean c) {
		this.isColliding = c;
	}
	
	public AABB getBB() {
		return this.bb;
	}
	
	public float getLifes() {
		return this.lifes;
	}
	
	public void setLifes(float lifes) {
		if(lifes > 1) {
			lifes = 1;
		}
		
		this.lifes = lifes;
	}
	
	public void hurt(float dmg) {	
		this.lifes -= dmg;
		if(this.lifes < 0) {
			this.lifes = 0;
		}
		
	}
	
	public void hurt(int dmg) {
		this.hurt(dmg / 100f);
	}
	
	public boolean shouldRemove() {
		return (this.lifes <= 0);
	}
	
	public boolean isColliding(Entity e) {
		return this.isColliding || (e.getBB().intersects(this.bb));
	}
	
	public abstract void update(World world);
	public abstract void render(float ptt, World world);

	public AABB getLastBB() {
		return lastBB;
	}

	public AABB getRenderBB() {
		return renderBB;
	}
}
