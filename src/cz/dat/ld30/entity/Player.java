package cz.dat.ld30.entity;

import java.util.HashMap;
import java.util.Map;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import cz.dat.ld30.GameVariables;
import cz.dat.ld30.util.AABB;
import cz.dat.ld30.util.Coord2D;
import cz.dat.ld30.util.GLUtil;
import cz.dat.ld30.world.Block;
import cz.dat.ld30.world.World;

public class Player extends Entity {

	private int texID = 7;
	private int currentImage = 1;
	
	private int groundImagesStart = 2;
	private int groundImagesEnd = 9;
	
	private float speed = 0.25f;
	private float jumpForce = 0.8f;

	private float velX;
	private float velY;

	private boolean facingRight = true;
	private boolean onGround = true;
	
	private int standingOnX;
	private int standingOnY;
	private int standingOnID;

	private float timer;
	private Map<Coord2D, Integer> states = new HashMap<>();
	private int collectedCoins = 0;

	public Player(int x, int y) {
		super(new AABB(x, y-2, x+0.5f, y));
	}

	@Override
	public void update(World world) {

		this.lastBB = this.bb.clone();
		this.checkBlocks(world);
		
		if(Keyboard.isKeyDown(Keyboard.KEY_A) || Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
			this.velX -= speed;
			this.facingRight = false;
		}

		if(Keyboard.isKeyDown(Keyboard.KEY_D) || Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
			this.velX += speed;
			this.facingRight = true;
		}

		if((Keyboard.isKeyDown(Keyboard.KEY_SPACE) || Keyboard.isKeyDown(Keyboard.KEY_W) || Keyboard.isKeyDown(Keyboard.KEY_UP)) && this.onGround) {
				this.velY -= this.jumpForce;
				this.onGround = false;
				world.getGame().getSoundManager().playSound("jump");
		}
		
		this.velX *= 0.75f;

		velY += 0.1f;
		
		if (Math.abs(velX) > 0.01) {
			
			this.timer += this.onGround ? 1 : 0.2f;

		}
		
		if(timer >= 2) {
			timer = 0;
			
				this.currentImage++;
				if(this.currentImage > this.groundImagesEnd) {
					this.currentImage = this.groundImagesStart;
				}
		}

		float[] clipped = this.bb.moveCollide(world, velX, velY);
		
		if(velY != clipped[1]) {
			this.onGround = velY > 0;
			this.velY = 0;
		} else {
			this.onGround = false;
		}
		
		if(velX != clipped[0]) {
			this.velX = 0;
		}
		
		if(this.bb.y0 > world.getHeight()) {
			this.setLifes(0);
		}

	}
	
	public void nullVelocity() {
		this.velX = 0;
	}
	
	private void checkBlocks(World world) {
		int underX = (int)(this.bb.x1 + 0.5f);
		int underY = ((int)this.bb.y1 + 1);
		int blockUnder = world.getBlock(underX, underY);
		
		this.standingOnX = underX;
		this.standingOnY = underY;
		this.standingOnID = blockUnder;
		
		if(blockUnder == Block.bouncy.getID()) {
			this.jumpForce = 1.8f;
		} else {
			this.jumpForce = 0.8f;
		}
		
		if(blockUnder == Block.ice.getID()) {
			this.updateIce(underX, underY, world);
		}
		
		int minX = (int) (this.bb.x0)+1;
		int maxX = (int) (this.bb.x1+1f)+1;
		
		int minY = (int) this.bb.y0+1;
		int maxY = (int) this.bb.y1+2;
		
		for (int x = minX; x < maxX; x++) {
			for (int y = minY; y < maxY; y++) {
				int blockID = world.getBlock(x, y);
				
				if (blockID == Block.coin.getID()) {
					world.setBlock(x, y, 0);
					this.collectedCoins++;
					world.getGame().getSoundManager().playSound("coin");
				}
				
				if(blockID == Block.tnt.getID()) {
					world.setBlock(x, y, 0);
					this.hurt(50);
					world.getGame().getSoundManager().playSound("expl");
				}
			}
		}
		
		
	}
	
	
	private void updateIce(int x, int y, World w) {
		Coord2D c = new Coord2D(x, y);
		Integer st = this.states.get(c);
		int newSt;
		
		if(st == null) {
			this.states.put(c, 10);
			return;
		} else {
			newSt = st - 1;
			this.states.put(c, newSt);
		}
		
		if(newSt <= 0) {
			this.states.remove(c);
			w.setBlock(x, y, 0);
			w.getGame().getSoundManager().getSoundSystem().play("icebreak");
		}
	}

	@Override
	public void render(float ptt, World world) {		
		boolean flip = this.facingRight;
		
		int bs = GameVariables.blockSize.getActualValue();
		
		GL11.glPushMatrix();
		GL11.glTranslatef(+0.5f*bs, 0, 0);
		
		if (flip) {
			GLUtil.drawFromAtlas(world.getGame().getTextureManager(), this.texID,
				this.currentImage, (int) (super.renderBB.x0 * bs - bs *0.25), (int) (super.renderBB.x1 * bs + bs + bs *0.25), (int) (super.renderBB.y0*bs+bs), (int) (super.renderBB.y1*bs+bs));
		} else {
			GLUtil.drawFromAtlas(world.getGame().getTextureManager(), this.texID,
				this.currentImage, (int) (super.renderBB.x1 * bs + bs + bs *0.25), (int) (super.renderBB.x0 * bs - bs *0.25), (int) (super.renderBB.y0*bs+bs), (int) (super.renderBB.y1*bs+bs));
		}

		GL11.glPopMatrix();
		
	}

	@Override
	public void beforeRender(float ptt) {
		this.renderBB = AABB.mix(this.lastBB, this.bb, ptt);
	}

	public int getStandingOnX() {
		return standingOnX;
	}

	public int getStandingOnY() {
		return standingOnY;
	}

	public int getStandingOnID() {
		return standingOnID;
	}
	
	public int getCoinsFlush() {
		int b = this.collectedCoins;
		this.collectedCoins = 0;
		return b;
	}

}
